#include "learningwindow.h"
#include "ui_learningwindow.h"
#include "mainwindow.h"
#include "entrywindow.h"

LearningWindow::LearningWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LearningWindow)
{
    ui->setupUi(this);

    updateButtons();

    connect(
        ui->nextButton, &QPushButton::clicked,
        this, &LearningWindow::nextPage
    );

    connect(
        ui->previousButton, &QPushButton::clicked,
        this, &LearningWindow::previousPage
    );

    connect(
        ui->resetButton, &QPushButton::clicked,
        []()
        {
            MainWindow::active()->setContent( new EntryWindow() );
        }
    );


    labels    << "Periferie počítače"
              << "Klávesnice"
              << "Myš"
              << "Mikrofon"
              << "Reproduktor"
              << "Monitor"
              << "Tiskárna"
              << "Tablet & Trackball"
    ;
}

LearningWindow::~LearningWindow()
{
    delete ui;
}

void LearningWindow::updateButtons()
{
    ui->previousButton->setEnabled( page > 0 );
    ui->nextButton->setEnabled( page < PAGES - 1 );
}

void LearningWindow::setPage(int page)
{
    ui->label->setText(QString::number(page));
    updateButtons();
//    ui->textBrowser->setSource(QUrl(QStringLiteral("qrc:/html/1.html")));

    auto html = QStringLiteral("qrc:/html/%1.html").arg(page+1);
    ui->textBrowser->setSource(QUrl(html));
    ui->label->setText(labels.at(page));
}

void LearningWindow::nextPage()
{
    if( page + 1 >= PAGES )
    {

    }
    else
    {
        page++;
    }

    setPage(page);
}

void LearningWindow::previousPage()
{
    if( page - 1 < 0 )
    {

    }
    else
    {
        page--;
    }

    setPage(page);
}
