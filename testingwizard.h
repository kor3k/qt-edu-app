#ifndef TESTINGWIZARD_H
#define TESTINGWIZARD_H

#include <QWizard>

namespace Ui {
class TestingWizard;
}

class TestingWizard : public QWizard
{
    Q_OBJECT

public:
    explicit TestingWizard(QWidget *parent = 0);
    ~TestingWizard();

public Q_SLOTS:
    void    accept();
    void    reject();

private:
    Ui::TestingWizard *ui;
};

#endif // TESTINGWIZARD_H
