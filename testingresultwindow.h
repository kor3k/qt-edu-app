#ifndef TESTINGRESULTWINDOW_H
#define TESTINGRESULTWINDOW_H

#include <QWidget>

namespace Ui {
class TestingResultWindow;
}

class TestingResultWindow : public QWidget
{
    Q_OBJECT

public:
    explicit TestingResultWindow(int points, QWidget *parent = 0);
    ~TestingResultWindow();

private:
    Ui::TestingResultWindow *ui;
    int points;
};

#endif // TESTINGRESULTWINDOW_H
