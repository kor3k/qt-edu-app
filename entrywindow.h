#ifndef ENTRY_H
#define ENTRY_H

#include <QMouseEvent>
#include <QWidget>

namespace Ui {
class EntryWindow;
}

class EntryWindow : public QWidget
{
    Q_OBJECT

public:
    explicit EntryWindow(QWidget *parent = 0);
    ~EntryWindow();

public Q_SLOTS:
    void showTestingWizard();
    void showLearningWindow();
    void maximizeMainWindow();

private:
    Ui::EntryWindow *ui;
};

#endif // ENTRY_H
