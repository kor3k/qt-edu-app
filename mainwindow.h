#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void setContent( QWidget *content );

    static MainWindow* active();

public Q_SLOTS:
    void showAbout();

private:
    Ui::MainWindow *ui;
    QWidget *content = nullptr;
};

#endif // MAINWINDOW_H
