#include "entrywindow.h"
#include "ui_entrywindow.h"
#include <QDebug>
#include "mainwindow.h"
#include "learningwindow.h"
#include "testingwizard.h"

EntryWindow::EntryWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::EntryWindow)
{
    ui->setupUi(this);

    connect(ui->quitButton, SIGNAL (clicked()), qApp, SLOT (quit()));

    //connect(ui->quitButton, SIGNAL ( mouseReleaseEvent(QMouseEvent *) ), qApp, SLOT (quit()));
    /*
    connect(
        ui->quitButton, &QPushButton::clicked,
        this, &EntryWindow::clickHandler
    );
*/



    connect(
        ui->learningButton, &QPushButton::clicked,
        this, &EntryWindow::maximizeMainWindow
    );

    connect(
        ui->learningButton, &QPushButton::clicked,
        this, &EntryWindow::showLearningWindow
    );

    connect(
        ui->testingButton, &QPushButton::clicked,
        this, &EntryWindow::maximizeMainWindow
    );

    connect(
        ui->testingButton, &QPushButton::clicked,
        this, &EntryWindow::showTestingWizard
    );


    auto w = MainWindow::active();
    connect(
        ui->infoButton, &QPushButton::clicked,
        w, &MainWindow::showAbout
    );

}

void EntryWindow::showLearningWindow()
{
    auto w = MainWindow::active();
    w->setContent( new LearningWindow(w) );
}

void EntryWindow::showTestingWizard()
{
    auto w = MainWindow::active();
    w->setContent( new TestingWizard(w) );
}

void EntryWindow::maximizeMainWindow()
{
    MainWindow* w = MainWindow::active();
    w->setWindowState(w->windowState() | Qt::WindowMaximized);
    //w->showMaximized();

    qDebug() << "is_maximized: " << w->isMaximized();

    //w->setWindowState(Qt::WindowMaximized);
//    w->close();
    //w->setContent();
    //qApp->activeWindow()
}

EntryWindow::~EntryWindow()
{
    delete ui;
}
