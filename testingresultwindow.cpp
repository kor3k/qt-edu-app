#include "testingresultwindow.h"
#include "ui_testingresultwindow.h"
#include "mainwindow.h"
#include "entrywindow.h"

TestingResultWindow::TestingResultWindow(int points, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TestingResultWindow) ,
    points(points)
{
    ui->setupUi(this);

    ui->label_points->setText(QString::number(points));

    connect(
        ui->resetButton, &QPushButton::clicked,
        []()
        {
            MainWindow::active()->setContent( new EntryWindow() );
        }
    );
}

TestingResultWindow::~TestingResultWindow()
{
    delete ui;
}
