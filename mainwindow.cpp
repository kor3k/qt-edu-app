#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    this->ui->centralWidget->layout()->setMenuBar(ui->menuBar);

//    connect(ui->menuExit, SIGNAL (aboutToShow()), qApp, SLOT (quit()));
    connect(
        ui->menuExit, &QMenu::aboutToShow,
        qApp, &QApplication::quit
    );

    connect(
        ui->menuInfo, &QMenu::aboutToShow,
        this, &MainWindow::showAbout
    );
}

MainWindow::~MainWindow()
{
    delete ui;
    delete content;
}

void MainWindow::setContent(QWidget *content)
{
    if(this->content)
        this->content->hide();
    delete this->content;

    this->content   =   content;
    this->ui->centralWidget->layout()->addWidget(content);
//    content->show();
}

MainWindow* MainWindow::active()
{
    return dynamic_cast<MainWindow*>(qApp->activeWindow());
}

void MainWindow::showAbout()
{
    auto w = MainWindow::active();

    //steals focus -> changes active window
    QMessageBox::about( this->ui->centralWidget , "o programu" , "Výukový program - Martin Tuček - martin.tucek@gmail.com" );

    qApp->setActiveWindow(w);
}
