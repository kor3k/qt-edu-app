#include "mainwindow.h"
#include "entrywindow.h"
#include <QApplication>
#include <QDebug>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    a.setActiveWindow(&w);
    //a.activeWindow()->close();

    w.setContent( new EntryWindow(&w) );

    //Entry e(&w);
    //e.show();
    //e.hide();

    qDebug() << QT_VERSION_STR;

    return a.exec();
}
