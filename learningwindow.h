#ifndef LEARNINGWINDOW_H
#define LEARNINGWINDOW_H

#include <QWidget>

namespace Ui {
class LearningWindow;
}

class LearningWindow : public QWidget
{
    Q_OBJECT

public:
    explicit LearningWindow(QWidget *parent = 0);
    ~LearningWindow();

    void    updateButtons();
    void    nextPage();
    void    previousPage();

private:
    Ui::LearningWindow *ui;
    static const int PAGES = 8;
    int page = 0;
    QStringList labels;

    void    setPage(int page);
};

#endif // LEARNINGWINDOW_H
