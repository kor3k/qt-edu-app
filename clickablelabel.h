//
// Created by kor3k on 21.3.18.
//

#ifndef UNTITLED_CLICKABLELABEL_H
#define UNTITLED_CLICKABLELABEL_H

#include <QLabel>
#include <QWidget>
#include <Qt>
#include <QDebug>
#include <QObject>


class ClickableLabel : public QLabel {
Q_OBJECT

public:
    explicit ClickableLabel(QWidget* parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags());
    virtual ~ClickableLabel() override;

signals:
    void clicked();

protected:
    void mousePressEvent(QMouseEvent* event);

};

//https://wiki.qt.io/Clickable_QLabel
#endif //UNTITLED_CLICKABLELABEL_H
