#include "testingwizard.h"
#include "ui_testingwizard.h"
#include <QDebug>
#include "mainwindow.h"
#include "entrywindow.h"
#include "testingresultwindow.h"

TestingWizard::TestingWizard(QWidget *parent) :
    QWizard(parent),
    ui(new Ui::TestingWizard)
{
    ui->setupUi(this);

    //ui->wizardPage_1->registerField("ans_1_1",ui->radioButton_1_1);
    //RadioButton->isChecked()
}

TestingWizard::~TestingWizard()
{
    delete ui;
}

void TestingWizard::reject()
{
    qDebug() << "rejecting";

    QDialog::reject();

    MainWindow::active()->setContent( new EntryWindow() );
}

void TestingWizard::accept()
{
    qDebug() << "accepting";

    int points  =   0;

    if( ui->radioButton_1_2->isChecked() )
        points++;

    if( ui->radioButton_2_1->isChecked() )
        points++;

    if( ui->radioButton_3_3->isChecked() )
        points++;

    if( ui->radioButton_4_3->isChecked() )
        points++;

    if( ui->radioButton_5_1->isChecked() )
        points++;

    if( ui->radioButton_6_1->isChecked() )
        points++;

    if( ui->radioButton_7_3->isChecked() )
        points++;

    if( ui->radioButton_8_1->isChecked() )
        points++;

    if( ui->radioButton_9_1->isChecked() )
        points++;

    if( ui->radioButton_10_2->isChecked() )
        points++;

    qDebug() << "points: " << points;

    QDialog::accept();

    MainWindow::active()->setContent( new TestingResultWindow(points) );
}
